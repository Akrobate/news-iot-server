'use strict';

const {
    expect,
} = require('chai');
const {
    stub,
} = require('sinon');
const superTest = require('supertest');

const {
    app,
} = require('../../src/server-app');

const {
    ServiceLoader,
    NewsService,
} = require('../../src/services');

describe('NewsController', () => {

    let service_loader_stub = null;

    before(() => {
        service_loader_stub = stub(ServiceLoader, 'getServiceClass')
            .callsFake(() => NewsService);
    });

    after(() => {
        service_loader_stub.restore();
    });

    it('Should be able to get news', (done) => {
        superTest(app)
            .get('/api/news')
            .expect(200)
            .end((error, response) => {
                if (error) {
                    done(error)
                }
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('Array');
                done();
            })
    });

    it('Should be able to get breaking news', (done) => {
        superTest(app)
            .get('/api/breaking-news')
            .expect(200)
            .end((error, response) => {
                if (error) {
                    done(error)
                }
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('Array');
                done();
            });
    });

    it('Should be able acknowledge breaking news', (done) => {
        superTest(app)
            .post('/api/breaking-news/acknowledge')
            .expect(200)
            .end((error, response) => {
                if (error) {
                    done(error)
                }
                done();
            });
    });
})