'use strict';

const axios = require('axios');

const {
    NewsService,
} = require('../../../src/services/custom/NewsService');
const {
    mock
} = require('sinon');

describe('Custom NewsService', () => {

    const mocks = {};

    beforeEach(() => {
        mocks.axios = mock(axios);
    });

    it('Check fetch rss news', async () => {
        mocks.axios.expects('post')
            .once()
            .resolves({ data: {} });

        const news_service = NewsService.getInstance();
        const data = await news_service.fetchRssNews();
        console.log(data);
    });
});
