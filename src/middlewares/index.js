'use strict';

const {
    access_control_allow_middleware,
} = require('./HttpAccessControlAllow');
const {
    express_json,
    express_urlencoded_middleware,
    cors_middleware,
} = require('./HttpGenericExpress');
const {
    error_manager_middleware,
    not_found_error_middleware,
} = require('./HttpErrorManager');
const {
    http_logger_middleware,
} = require('./HttpLogger')

module.exports = {
    cors_middleware,
    error_manager_middleware,
    express_json,
    access_control_allow_middleware,
    express_urlencoded_middleware,
    not_found_error_middleware,
    http_logger_middleware,
}
