'use strict';

var morgan = require('morgan');

const http_logger_middleware = morgan('combined');

module.exports = {
    http_logger_middleware,
}