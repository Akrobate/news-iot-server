'use strict';

class NewsService {

    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (NewsService.instance === null) {
            NewsService.instance = new NewsService();
        }
        return NewsService.instance;
    }

    /**
     * @returns {Object}
     */
    getNews(_) {
        const news_list = [
            {
                content: 'Le télétravail reste la règle en période de pandémie'
            },
            {
                content: 'Dans un contrat d’assurance, une clause d’exclusion de garantie imprécise ne peut recevoir application'
            },
            {
                content: 'Pinterest lance un nouveau format : les « Épingles Story »'
            },
            {
                content: 'Sélection : Mondiaux Jeunes Juniors de Biathlon'
            },
            {
                content: 'Le SAAP innove'
            },
            {
                content: 'William Berger rejoint Eight Advisory en tant qu’associé au sein du pôle Transformation'
            },
            {
                content: 'Orientation, le SIJ au service des jeunes'
            },
            {
                content: 'Atelier, docteurs et doctorants en SHS'
            },
            {
                content: 'Les journaux locaux américains condamnés à disparaître ?'
            },
            {
                content: 'Stage Assistant Communication 360°'
            },
        ];

        return Promise.resolve(news_list);
    }

    /**
     * @returns {Object}
     */
    getBreakingNews(input) {
        const news_list = [
            {
                content: 'Breaking news Le télétravail reste la règle en période de pandémie'
            },
            {
                content: 'Breaking news Dans un contrat d’assurance, une clause d’exclusion de garantie imprécise ne peut recevoir application'
            },
            {
                content: 'Breaking news Pinterest lance un nouveau format : les « Épingles Story »'
            },
            {
                content: 'Breaking news Sélection : Mondiaux Jeunes Juniors de Biathlon'
            },
            {
                content: 'Breaking news Le SAAP innove'
            },
            {
                content: 'Breaking news William Berger rejoint Eight Advisory en tant qu’associé au sein du pôle Transformation'
            },
            {
                content: 'Breaking news Orientation, le SIJ au service des jeunes'
            },
            {
                content: 'Breaking news Atelier, docteurs et doctorants en SHS'
            },
            {
                content: 'Breaking news Les journaux locaux américains condamnés à disparaître ?'
            },
            {
                content: 'Breaking news Stage Assistant Communication 360°'
            },
        ];

        return Promise.resolve(news_list)
    }

    
    /**
     * @returns {Promise}
     */
    acknowledgeBreakingNews() {
        return Promise.resolve();
    }

}

NewsService.instance = null;

module.exports = {
    NewsService,
};
