'use strict';

const {
    NewsService,
} = require('./NewsService');

const {
    ServiceLoader,
} = require('./ServiceLoader');

module.exports = {
    NewsService,
    ServiceLoader,
};
