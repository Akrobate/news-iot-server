const fs = require('fs');
const {
    logger,
} = require('../logger');


class ServiceLoader {

    static getServiceClass(service_name) {
        let service_class;
        try {
            if(fs.existsSync(`${__dirname}/custom/${service_name}.js`)) {
                service_class = require(`./custom/${service_name}`)[service_name];
            } else {
                service_class = require(`./${service_name}`)[service_name];
            }
        } catch (err) {
            logger.error(err);
        }

        return service_class;
    }
}

module.exports = {
    ServiceLoader,
};
