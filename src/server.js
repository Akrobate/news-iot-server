'use strict';

const {
    app,
} = require('./server-app');
const {
    logger,
} = require('./logger');
const {
    configuration,
} = require('./configuration');

app.disable('x-powered-by');

app.listen(configuration.port, () => {
    logger.log('Server started');
});
