'use strict';

const express = require('express');

const {
    base_url,
    route_collection,
} = require('./router');

const {
    access_control_allow_middleware,
    error_manager_middleware,
    not_found_error_middleware,
    express_json,
    express_urlencoded_middleware,
    cors_middleware,
    http_logger_middleware,
} = require('./middlewares');

const app = express();

app.use(http_logger_middleware);
app.use(cors_middleware);
app.use(access_control_allow_middleware);
app.use(express_json);
app.use(express_urlencoded_middleware);
app.use(base_url, route_collection);
app.use(not_found_error_middleware);
app.use(error_manager_middleware);

module.exports = {
    app,
};
