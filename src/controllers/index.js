'use strict';

const {
    NewsController,
} = require('./NewsController');

module.exports = {
    NewsController,
};
