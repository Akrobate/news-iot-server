'use strict';

const {
    ServiceLoader,
} = require('../services');

class NewsController {

    // eslint-disable-next-line require-jsdoc
    constructor(news_service) {
        this.news_service = news_service;
    }

    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (NewsController.instance === null) {
            NewsController.instance = new NewsController(
                ServiceLoader.getServiceClass('NewsService').getInstance()
            );
        }
        return NewsController.instance;
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Object}
     */
    async getNewsIot(request, response) {
        const input = request;
        const result = await this.news_service.getNews(input);
        return response.send(result);
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Object}
     */
     async getBreakingNewsIot(request, response) {
        const input = request;
        const result = await this.news_service.getBreakingNews(input);
        return response.send(result);
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Object}
     */
     async postBreakingNewsAcknowledgeIot(request, response) {
        const input = request;
        await this.news_service.acknowledgeBreakingNews(input)
        return response.send({
            status: 'ok',
        });
    }

}

NewsController.instance = null;

module.exports = {
    NewsController,
};
