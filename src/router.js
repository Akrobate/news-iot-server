'use strict';

const http_status = require('http-status');

const {
    Router,
} = require('express');

const {
    NewsController,
} = require('./controllers');
const base_url = '/api';
const route_collection = Router(); // eslint-disable-line new-cap

const news_controller = NewsController.getInstance();

route_collection.get(
    '/ping',
    (_, response) => response.status(http_status.OK).send({
        pong: true,
    }));

route_collection.get(
    '/news',
    (request, response) => news_controller.getNewsIot(request, response));

route_collection.get(
    '/breaking-news',
    (request, response) => news_controller.getBreakingNewsIot(request, response));

route_collection.post(
    '/breaking-news/acknowledge',
    (request, response) => news_controller.postBreakingNewsAcknowledgeIot(request, response));

module.exports = {
    base_url,
    route_collection,
};
